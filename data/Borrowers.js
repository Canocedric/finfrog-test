class Borrowers {
    constructor(dao) {
      this.dao = dao;
    }
    
    getBorrowerById(id) {
        return this.dao.get(
          `SELECT * FROM borrowers WHERE id = ?`,
          [id]);
    }
}

module.exports = Borrowers;