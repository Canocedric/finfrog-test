const sqlite3 = require('sqlite3');
const DBFILEPATH = './data/finfrog_loans.db';

class AppDAO {
    constructor() {
      this.db = new sqlite3.Database(DBFILEPATH, (err) => {
        if (err) {
          console.log('Could not connect to database', err);
        } else {
          console.log('Connected to database');
        }
      })
    }

    get(sql, params = []) {
        return new Promise((resolve, reject) => {
          this.db.get(sql, params, (err, result) => {
            if (err) {
              console.log('Error running sql: ' + sql);
              console.log(err);
              reject(err);
            } else {
              resolve(result);
            }
          })
        })
      }
    
      run(sql, params = []) {
        return new Promise((resolve, reject) => {
          this.db.all(sql, params, (err) => {
            if (err) {
              console.log('Error running sql: ' + sql);
              console.log(err);
              reject(err);
            } else {
              resolve();
            }
          })
        })
      }
  }
  
  module.exports = AppDAO