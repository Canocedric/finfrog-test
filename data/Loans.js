class Loans {
    constructor(dao) {
      this.dao = dao;
    }
    
    getLastRecord() {
        return this.dao.get(`SELECT * FROM loans ORDER BY id DESC LIMIT 1`);
    }

    recordLoans({borrower_id, currency, amount_cents}) {
        return this.dao.run(`
        INSERT INTO loans (borrower_id, currency, amount_cents)
        VALUES(${borrower_id} , "${currency}", ${amount_cents})`);
    }
}

module.exports = Loans;