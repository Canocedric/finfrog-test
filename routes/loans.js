var express = require('express');
var router = express.Router();

const LoansController = require('../controllers/loansController');

/* GET users listing. */
// router.get('/', UserController.getUsers);

/* POST a loan. */
router.post('/', LoansController.grantLoan);

module.exports = router;
