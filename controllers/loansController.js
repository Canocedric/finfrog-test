const AppDAO = require('../data/dao');
const Loans = require('../data/Loans');
const Borrowers = require('../data/Borrowers');

const appDAO = new AppDAO();
const loans = new Loans(appDAO);
const borrowers = new Borrowers(appDAO);

const grantLoan = async ({body}, res) => {
    const {borrower_id, amount_cents, currency} = body;

    if (amount_cents <= 0) return res.status(400).end();
    
    if (!(borrower_id % 2)) return res.status(422).end();

    const borrower = await borrowers.getBorrowerById(borrower_id);

    if (!borrower) return res.status(400).end();

    await loans.recordLoans({borrower_id, currency, amount_cents});

    return res.status(201).end();
}

module.exports = {
    grantLoan,
}