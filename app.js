var express = require('express');
var logger = require('morgan');

var loansRouter = require('./routes/loans');


var app = express();

app.use(logger('dev'));
app.use(express.json());

app.use('/api/v1/loans', loansRouter);

module.exports = app;
