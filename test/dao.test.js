const AppDAO = require('../data/dao');
const Loans = require('../data/Loans');
const Borrowers = require('../data/Borrowers');

const appDAO = new AppDAO();
const loans = new Loans(appDAO);
const borrowers = new Borrowers(appDAO);

describe('Database tests', () => {
    it('should find a borrower in borrowers table if the specified id exists', async () => {
      const id = 1;
      const borrower = await borrowers.getBorrowerById(id);
      expect(borrower).toBeDefined();
      expect(borrower.id).toBe(id);
    });
    it('should not find a borrower in borrowers table if the specified id doesn\’t exist', async () => {
        const id = 'falseId';
        const borrower = await borrowers.getBorrowerById(id);
        expect(borrower).toBeUndefined();
    });
    it('should create a laon in laons table', async () => {
        let lastRecord = await loans.getLastRecord();
        let lastIdRecord = lastRecord ? lastRecord.id : 0;
        let newIdRecord = lastIdRecord + 1;
        const params = {
            "borrower_id": "2",
            "amount_cents": "20000",
            "currency": "EUR",
            "type": "PERSONAL"
        };
        await loans.recordLoans(params);
        lastRecord = await loans.getLastRecord();
        lastIdRecord = lastRecord.id;
        expect(newIdRecord).toBe(lastIdRecord);
    });
})