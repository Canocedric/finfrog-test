const request = require('supertest');

const app = require('../app');

const payload = {
    "borrower_id": "1",
    "amount_cents": "20000",
    "currency": "EUR",
    "type": "PERSONAL",
};

describe('End-to-end test', () => {
    it('should return a 201 HTTP response if loan is granted', async () => {
        const res = await request(app)
          .post('/api/v1/loans')
          .send(payload);
        expect(res.statusCode).toEqual(201);
    });
    it('should return a 422 HTTP response if loan is not granted', async () => {
        const res = await request(app)
          .post('/api/v1/loans')
          .send({...payload, "borrower_id": "2"});
        expect(res.statusCode).toEqual(422);
    });
    it('should return a 400 HTTP response if a bad borrower id is sent', async () => {
        const res = await request(app)
          .post('/api/v1/loans')
          .send({...payload, "borrower_id": "-1"});
        expect(res.statusCode).toEqual(400);
    });
    it('should return a 400 HTTP response if a bad amount_cents id is sent', async () => {
        const res = await request(app)
          .post('/api/v1/loans')
          .send({...payload, "amount_cents": "-3"});
        expect(res.statusCode).toEqual(400);
    });
})