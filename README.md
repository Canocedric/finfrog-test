﻿# FinFrog test

Test réalisé avec express.
La structure est assez classique.
Le controller loans est utilisé pour la logique d'acceptation du prêt ou non (dossier controllers).
La base de donnée est gérée via un dao assez simple (dossier data)
Deux type de test sont proposés, un fichier de test dao qui test la base de donnée et un fichier de test end-to-end qui test les appels http et les réponses http (dossier test).


# Installation

Un simple "npm install" suffit à installer les dépendances.

# Tests

Les tests sont réalisés avec jest et supertest, un simple "npm test" suffit à les réaliser.

# Utilisation

Vous pouvez utiliser la solution via un simple logiciel de requête HTTP type POSTMAN.
